var net = require('net');

var chatServer = net.createServer(),
	clientList = [];

chatServer.on('connection', function(client) {
	client.name = client.remoteAddress + ':' + client.remotePort;
	client.write('Hi ' + client.name + '!\n');
	console.log(client.name + ' joined. writable: ' + client.writable);

	// add new client to client list
	clientList.push(client);
	console.log('index number: ' + clientList.indexOf(client));

	// when a client sends new data, broadcast it
	client.on('data', function(data) {
		broadcast(data, client);
	});
	
	// when a client ends, remove it from the client list
	client.on('end', function() {
		console.log(client.name + ' quit');
		clientList.splice(clientList.indexOf(client), 1)
	});

	// logging errors
	client.on('error', function(e) {
		console.log(e)
	});
});

// Broadcast function that sends message from a client to others
function broadcast(message, client) {
	var cleanup = [];

	for(var i=0; i<clientList.length; i+=1) {
		console.log('index: ' + i + ', client: ' + clientList[i].name);
		if(client !== clientList[i]) {
			// check whether a client can be write to
			if(clientList[i].writable) {
				console.log(clientList[i].name + ' is writeable');
				clientList[i].write(client.name + ' says ' + message);
			// if can't, add to cleanup list and close it
			} else {
				console.log('non-writable. add ' + clientList[i].name + ' to clean up list');
				cleanup.push(clientList[i]);
				clientList[i].destroy();
			};
		};
	};

	// remove dead Nodes our of write loop to avoid trashing loop index
	for(i=0; i<cleanup.length; i+=1) {
		console.log('clean up ' + clientList[i].name);
		clientList.splice(clientList.indexOf(cleanup[i]), 1);
	};
};

chatServer.listen(9000);
